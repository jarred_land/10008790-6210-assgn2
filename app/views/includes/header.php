<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="<?php echo URLROOT . 'css/main.css'; ?>">
    
    <title>Making sense of Microtonality</title>

    <?php header("Content-Type: text/html; charset=ISO-8859-1"); ?>
</head>
<body>
    <div class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <div class="imgclass">
                <a href="index.html"><img src="<?php echo URLROOT . 'images/Placeholder.jpg'; ?>" alt="placeholder image" width="150" height="150"></a>
                </div>

                <?php include (APPROOT . '/views/includes/nav.php'); ?>

            </div>
        </div>
    </div>

    <div class="rectangle1">

    </div>

    <div class="maintextindex">

    <script src="<?php echo URLROOT . 'public/scripts/validation.js'; ?>"></script> 