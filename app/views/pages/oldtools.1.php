<?php include(APPROOT . "/views/includes/header.php"); ?>

 <div class="maintextindex">
        <div class="tableclass">
            <table style="width:100%">
                    <tr>
                      <th>Tool Description</th>
                      <th>Screenshot</th> 
                    </tr>
                    <tr>
                      <td>Scala is a microtonal tool</td>
                      <td><img src="<?php echo URLROOT . 'images/scala.png'; ?>" alt="Scala" width="150" height="150"></td> 
                    </tr>
                    <tr>
                      <td>ZynAddSubFx is a microtonal tool</td>
                      <td><img src="<?php echo URLROOT . 'images/zyn.png'; ?>" alt="ZynAddSubFx" width="150" height="150"></td> 
                    </tr>
            </table>
        </div>
    </div>

<?php include(APPROOT . "/views/includes/footer.php"); ?>