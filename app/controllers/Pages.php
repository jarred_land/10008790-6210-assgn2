<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Pages extends Controller {

        public function __construct() {
            $this->home = $this->model('HOME_');
            $this->scaledata = $this->model('SDatabase_');
            $this->aboutdata = $this->model('ABOUT_');
            $this->tooldata = $this->model('TOOLS_');
        }

        public function index() {

            $data = [
                'text' => $this->home->getAllHomeData()
            ];

            $this->view('pages/index', $data);

        }

        public function about() {

            $data = [

                'text' => $this->aboutdata->getAllAboutData()

            ];

            $this->view('pages/about', $data);

        }

        public function tools() {

            $data = [

                'text' => $this->tooldata->getAllToolData()
                
            ];

            $this->view('pages/tools', $data);

        }

        public function database() {

            $data = [
                'text' => $this->scaledata->getAllScaleData()
            ];

            $this->view('pages/database', $data);

        }

        public function external() {

            $data = [
                'text' => $this->externallinks->getAllExternalLinks()
            ];

            $this->view('pages/links', $data);

        }

    }

?>