<?php

    class HOME_ {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }
        
        public function getAllHomeData() {
            $this->db->query('SELECT * FROM HomeData');
            return $this->db->resultSet();
        }

    }

?>