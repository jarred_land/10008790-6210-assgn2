<?php

    class SDatabase_ {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }
        
        public function getAllScaleData() {
            $this->db->query('SELECT * FROM ScaleTable');
            return $this->db->resultSet();
        }

    }

?>