<?php

    class EXTERNALS_ {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }
        
        public function getAllExternalLinks() {
            $this->db->query('SELECT * FROM ExternalLinks');
            return $this->db->resultSet();
        }

    }

?>