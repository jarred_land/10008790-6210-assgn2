<?php

    class TOOLS_ {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }
        
        public function getAllToolData() {
            $this->db->query('SELECT * FROM ToolData');
            return $this->db->resultSet();
        }

    }

?>