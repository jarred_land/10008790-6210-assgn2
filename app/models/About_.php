<?php

    class ABOUT_ {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }
        
        public function getAllAboutData() {
            $this->db->query('SELECT * FROM AboutData');
            return $this->db->resultSet();
        }

    }

?>