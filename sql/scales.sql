USE containerdb;


DROP TABLE IF EXISTS ScaleTable;

CREATE TABLE ScaleTable (
    sclid INT(11) AUTO_INCREMENT, 
    Scale_Name VARCHAR(30),
    Scale_Description VARCHAR(255),
    Scale_Rating VARCHAR(10),
    PRIMARY KEY (sclid)
) AUTO_INCREMENT = 1; 

DROP TABLE IF EXISTS AuthorTable;

CREATE TABLE AuthorTable (
    authid INT(11) AUTO_INCREMENT, 
    Author VARCHAR(30),
    Scale_Produced VARCHAR(255),
    Contact_Information VARCHAR(100),
    PRIMARY KEY (authid)
) AUTO_INCREMENT = 1; 

DROP TABLE IF EXISTS HomeData;

CREATE TABLE HomeData (
    ID INT(11) AUTO_INCREMENT,
    CONTENT TEXT(5000),
    PRIMARY KEY (ID) 
) AUTO_INCREMENT = 1;


DROP TABLE IF EXISTS AboutData;

CREATE TABLE AboutData (
    ID INT(11) AUTO_INCREMENT,
    CONTENT TEXT(5000),
    PRIMARY KEY (ID) 
) AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS ToolData;

CREATE TABLE ToolData (
    ID INT(11) AUTO_INCREMENT,
    CONTENT TEXT(5000),
    PRIMARY KEY (ID) 
) AUTO_INCREMENT = 1;

CREATE TABLE ExternalLinks (
    ID INT(11) AUTO_INCREMENT,
    CONTENT TEXT(5000),
    PRIMARY KEY (ID) 
) AUTO_INCREMENT = 1;

INSERT INTO HomeData (CONTENT) VALUES ('Have you ever wondered why the music we hear every day is composed of the same 12 tones? Or why non-western music often sounds a bit strange 
        and off-key to most people raised under this system? The fact is, the frequencies that we use for the traditional 12 note per octave (or 12EDO 
        for short) system are very specific, yet there is a vast world of unused frequencies and harmonic relationships out there. We settled on the 
        12EDO standard, where C4 is always tuned to 261.63Hz (Hertz is the value used to measure frequency).
        <br><br>
        But what if instead, we used an alternate tuning? We can change the way the whole system works and sounds by simply inventing our own intervals 
        or generating them mathematically. This is microtonality, the practice of playing with intervals smaller than the traditional system. This allows
         us to reach tones and colors that simply cannot be achieved through 12EDO.
        <br><br>
        However, microtonality is something of a niche interest. We are raised on the western scale and most people never really question it because it’s 
        so ingrained into us. As such, the resources that exist for learning about microtonality and how to use it are often overly complicated and can 
        turn people off to the idea. The purpose of this website is to explain how and why microtonality works as well as providing information about software 
        that allows for microtonal tunings, as it’s thin on the ground. This website also offers a database of microtonal scales which, unlike actual songs, 
        don’t come with copyright issues.');

INSERT INTO AboutData (CONTENT) VALUES('This is the about page, which is visually identical to the home page. This text is here to 
            determine if it is working.');

INSERT INTO ToolData (CONTENT) VALUES ('The tool table has loaded');

INSERT INTO ExternalLinks (CONTENT) VALUES ('http://xenharmonic.wikispaces.com/')
INSERT INTO ExternalLinks (CONTENT) VALUES ('http://sevish.com/2015/make-a-new-musical-scale/')
INSERT INTO ExternalLinks (CONTENT) VALUES ('http://untwelve.org/what')

INSERT INTO ScaleTable (Scale_Name, Scale_Description, Scale_Rating) VALUES ('Bohlen-Pierce', 'Tritave scale based on dividing the octave into 13 1/2', '6/10');
INSERT INTO ScaleTable (Scale_Name, Scale_Description, Scale_Rating) VALUES ('Cactus Rosary', 'Just intonation scale using 12 pre-defined pitches', '8/10');
INSERT INTO ScaleTable (Scale_Name, Scale_Description, Scale_Rating) VALUES ('24EDO', '24 equal divisions of the octave, resulting in 24 notes per octave rather than 12', '9/10');
INSERT INTO ScaleTable (Scale_Name, Scale_Description, Scale_Rating) VALUES ('17EDO', '17 equal divisions of the octave, resulting in 17 notes per octave rather than 12', '8/10');

INSERT INTO AuthorTable (Author, Scale_Produced, Contact_Information) VALUES ('Heinz Bohlen, John Pierce', 'Bohlen-Pierce', 'placeholder@email.com');
INSERT INTO AuthorTable (Author, Scale_Produced, Contact_Information) VALUES ('Terry Riley', 'Cactus Rosary', 'placeholder@email.com');
INSERT INTO AuthorTable (Author, Scale_Produced, Contact_Information) VALUES ('No Author', '24EDO', 'placeholder@email.com');
INSERT INTO AuthorTable (Author, Scale_Produced, Contact_Information) VALUES ('No Author', '17EDO', 'placeholder@email.com');

SELECT * FROM ScaleTable;
SELECT * FROM AuthorTable;